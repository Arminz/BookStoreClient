package com.example.armin.bookstore;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by armin on 2/1/17.
 */
public interface ApiInterface {
    @GET("hello")
    Call<String> getHelloWorld();

}
