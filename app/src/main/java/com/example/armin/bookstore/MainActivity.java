package com.example.armin.bookstore;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }
    @OnClick(R.id.button)
    public void getHelloWorld()
    {
        Log.d("tag","button clicked");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(KEYS.URL.BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<String> call = service.getHelloWorld();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d(KEYS.LOG.RETROFIT.HELLO_WORLD, response.toString());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(KEYS.LOG.RETROFIT.HELLO_WORLD, "failed");

            }
        });
    }
}

